<?php

namespace app\controllers;

use app\models\P;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * PiezasController implements the CRUD actions for P model.
 */
class PiezasController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all P models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => P::find(), // select * from p;
           
            'pagination' => [
                'pageSize' => 4 // muestre los datos en 4 en 4 //
            ],
            'sort' => [
                'defaultOrder' => [
                    'P' => SORT_DESC,
                ]
            ],
          
        ]);

       // mostrar todas las piezas en la vista index 
        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single P model.
     * @param string $P P
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($P)
    {
        return $this->render('view', [
            'model' => $this->findModel($P),
        ]);
    }

    /**
     * Creates a new P model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new P();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'P' => $model->P]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing P model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $P P
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($P)
    {
        $model = $this->findModel($P);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'P' => $model->P]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing P model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $P P
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($P)
    {
        $this->findModel($P)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the P model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $P P
     * @return P the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($P)
    {
        if (($model = P::findOne(['P' => $P])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
