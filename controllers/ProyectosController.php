<?php

namespace app\controllers;

use app\models\J;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ProyectosController implements the CRUD actions for J model.
 */
class ProyectosController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all J models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => J::find(),
            
            'pagination' => [
                'pageSize' => 5
            ],
            'sort' => [
                'defaultOrder' => [
                    'j' => SORT_DESC,
                ]
            ],
            
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single J model.
     * @param string $j J
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($j)
    {
        return $this->render('view', [
            'model' => $this->findModel($j),
        ]);
    }

    /**
     * Creates a new J model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new J();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'j' => $model->j]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing J model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $j J
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($j)
    {
        $model = $this->findModel($j);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'j' => $model->j]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing J model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $j J
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($j)
    {
        $this->findModel($j)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the J model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $j J
     * @return J the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($j)
    {
        if (($model = J::findOne(['j' => $j])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
