<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "spj".
 *
 * @property string $s
 * @property string $p
 * @property string $j
 * @property int|null $cant
 *
 * @property J $j0
 * @property P $p0
 * @property S $s0
 */
class Spj extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'spj';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['s', 'p', 'j'], 'required'],
            [['cant'], 'integer'],
            [['s', 'p', 'j'], 'string', 'max' => 9],
            [['s', 'p', 'j'], 'unique', 'targetAttribute' => ['s', 'p', 'j']],
            [['j'], 'exist', 'skipOnError' => true, 'targetClass' => J::class, 'targetAttribute' => ['j' => 'j']],
            [['p'], 'exist', 'skipOnError' => true, 'targetClass' => P::class, 'targetAttribute' => ['p' => 'P']],
            [['s'], 'exist', 'skipOnError' => true, 'targetClass' => S::class, 'targetAttribute' => ['s' => 's']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            's' => 'Suministro',
            'p' => 'Piezas',
            'j' => 'Proyecto',
            'cant' => 'Cantidad',
        ];
    }

    /**
     * Gets query for [[J0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getJ0()
    {
        return $this->hasOne(J::class, ['j' => 'j']);
    }

    /**
     * Gets query for [[P0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getP0()
    {
        return $this->hasOne(P::class, ['P' => 'p']);
    }

    /**
     * Gets query for [[S0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getS0()
    {
        return $this->hasOne(S::class, ['s' => 's']);
    }
}
