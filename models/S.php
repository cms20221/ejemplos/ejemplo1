<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "s".
 *
 * @property string $s
 * @property string|null $noms
 * @property string|null $estado
 * @property string|null $ciudad
 *
 * @property Spj[] $spjs
 */
class S extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 's';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['s'], 'required'],
            [['s'], 'string', 'max' => 9],
            [['noms', 'estado', 'ciudad'], 'string', 'max' => 45],
            [['s'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            's' => 'codigo',
            'noms' => 'Nombre',
            'estado' => 'Estado',
            'ciudad' => 'Ciudad',
        ];
    }

    /**
     * Gets query for [[Spjs]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSpjs()
    {
        return $this->hasMany(Spj::class, ['s' => 's']);
    }
}
