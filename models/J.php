<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "j".
 *
 * @property string $j
 * @property string|null $nomj
 * @property string|null $ciudad
 * @property int|null $piezasTotales
 *
 * @property Spj[] $spjs
 */
class J extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'j';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['j'], 'required'],
            [['piezasTotales'], 'integer'],
            [['j'], 'string', 'max' => 9],
            [['nomj', 'ciudad'], 'string', 'max' => 45],
            [['j'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'j' => 'Codigo',//cambio j por codigo y nombre
            'nomj' => 'Nombre',
            'ciudad' => 'Ciudad',
            'piezasTotales' => 'Piezas Totales',
        ];
    }

    /**
     * Gets query for [[Spjs]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSpjs()
    {
        return $this->hasMany(Spj::class, ['j' => 'j']);
    }
}
