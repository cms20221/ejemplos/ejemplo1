<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "p".
 *
 * @property string $P
 * @property string|null $nomp
 * @property string|null $color
 * @property string|null $peso
 * @property string|null $ciudad
 * @property int|null $piezasPorColor
 * @property int|null $piezasPorCiudad
 *
 * @property Spj[] $spjs
 */
class P extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'p';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['P'], 'required'],
            [['piezasPorColor', 'piezasPorCiudad'], 'integer'],
            [['P'], 'string', 'max' => 9],
            [['nomp', 'color', 'peso', 'ciudad'], 'string', 'max' => 45],
            [['P'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'P' => 'Codigo',// modifico titulo campo
            'nomp' => 'Nombre',
            'color' => 'Color',
            'peso' => 'Peso',
            'ciudad' => 'Ciudad',
            'piezasPorColor' => 'Piezas Por Color',
            'piezasPorCiudad' => 'Piezas Por Ciudad',
        ];
    }

    /**
     * Gets query for [[Spjs]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSpjs()
    {
        return $this->hasMany(Spj::class, ['p' => 'P']);
    }
}
