<?php

use app\models\Spj;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Suministros';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="spj-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Crear suministro',
                ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            's',
            's0.noms',//quiero mostrar el nombre
            'p',
            'p0.nomp',//nombre pieza
            'j',
            'j0.nomj',// quiero mostrar el nombre del proyecto
            'cant',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, Spj $model, $key, $index, $column) {
                    return Url::toRoute([$action, 's' => $model->s, 'p' => $model->p, 'j' => $model->j]);
                 }
            ],
        ],
    ]); ?>


</div>
