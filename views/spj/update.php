<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Spj $model */

$this->title = 'Nombre Suministro: ' . $model->s;
$this->params['breadcrumbs'][] = ['label' => 'Spjs', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->s, 'url' => ['view', 's' => $model->s, 'p' => $model->p, 'j' => $model->j]];
$this->params['breadcrumbs'][] = 'actualizar';
?>
<div class="spj-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
