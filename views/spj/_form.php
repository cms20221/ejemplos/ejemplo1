<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\Spj $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="spj-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php
            //echo $form->field($model, 's')->textInput(['maxlength' => true]);
            //mostrar el control como caja de texto
    
            $suministradores= \app\models\S::find()->all(); //select * from s
            $items=\yii\helpers\ArrayHelper::map(
                                                   $suministradores,
                                                    's',
                                                    'noms'
                                                    );
            
            echo $form->field($model, 's')->dropDownList($items); // mostrar desplegable
    ?>

    <?php
       // echo $form->field($model, 'p')->textInput(['maxlength' => true]);
       // mostrar el control como caja de texto
        $piezas= \app\models\P::find()->all(); 
        $items= \yii\helpers\ArrayHelper::map($piezas, 'P', 'nomp'
                                                            );
         echo $form->field($model, 'p')->dropDownList($items);
    ?> 
    
     <?php
            //$form->field($model, 'j')->textInput(['maxlength' => true]);
            //mostrar el control como caja de texto
    
            $proyectos= \app\models\J::find()->all(); //select * from s
            $items=\yii\helpers\ArrayHelper::map(
                                                   $proyectos,
                                                    'j',
                                                    'nomj'
                                                    );
            
            echo $form->field($model, 'j')->dropDownList($items); // mostrar desplegable
    ?>
    
        
    
        
    
  

    <?= $form->field($model, 'cant')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Grabar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
