<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/** @var yii\web\View $this */
/** @var app\models\Spj $model */

$this->title = "Suministros" . $model->s;
$this->params['breadcrumbs'][] = ['label' => 'Suministro', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="spj-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Actualizar', ['update', 's' => $model->s, 'p' => $model->p, 'j' => $model->j], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Eliminar', ['delete', 's' => $model->s, 'p' => $model->p, 'j' => $model->j], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Estas seguro que quieres eliminarlo?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            's',
            'p',
            'j',
            'cant',
        ],
    ]) ?>

</div>
