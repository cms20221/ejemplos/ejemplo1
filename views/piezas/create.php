<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\P $model */

$this->title = 'Nueva Pieza'; // modificando el titulo de la pagina
$this->params['breadcrumbs'][] = [
    'label' => 'Piezas', // coloco el texto en las migas
    'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="p-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
