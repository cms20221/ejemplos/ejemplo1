<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/** @var yii\web\View $this */
/** @var app\models\P $model */

$this->title = "Detalles de la pieza " . $model->P; // cambiar titulo de la web
$this->params['breadcrumbs'][] = [
                                'label' => 'Piezas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="p-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(
                'Actualizar', [
                    'update', 'P' => $model->P], // accion del controlador
                    ['class' => 'btn btn-primary'] // estilo visual boton
                ) ?> 
        <?= Html::a(
                'Eliminar', //texto del boton
                ['delete', 'P' => $model->P], //accion del controlador
                [
                 'class' => 'btn btn-danger',// estilo visual
                    'data' => [
                'confirm' => 'Seguro que quieres eliminar la pieza?',// mensaje de confirmacion
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'P',
            'nomp',
            'color',
            'peso',
            'ciudad',
            'piezasPorColor',
            'piezasPorCiudad',
        ],
    ]) ?>

</div>
