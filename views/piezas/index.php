<?php

use app\models\P;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Piezas';//cambio el titulo de la web
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="p-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Nueva Pieza', // texto del boton
                ['create'],// accion del controlador 
                ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'P',
            'nomp',
            'color',
            'peso',
            'ciudad',
            //'piezasPorColor',
            //'piezasPorCiudad',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, P $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'P' => $model->P]);
                 }
            ],
        ],
    ]); ?>


</div>
