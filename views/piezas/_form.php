<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\P $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="p-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'P')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'nomp')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'color')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'peso')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'ciudad')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'piezasPorColor')->textInput() ?>

    <?= $form->field($model, 'piezasPorCiudad')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton(
                                'Grabar', // cambiar el texto del boton submit
                                ['class' => 'btn btn-success']
                                ) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
