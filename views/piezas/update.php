<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\P $model */

$this->title = 'Modificando la pieza: ' . $model->P; // titulo de la web 
$this->params['breadcrumbs'][] = [
    'label' => 'Piezas', // etiqueta de las migas
    'url' => ['index'] 
    ];
$this->params['breadcrumbs'][] = ['label' => $model->P, 'url' => ['view', 'P' => $model->P]];
$this->params['breadcrumbs'][] = 'Modificar'; // etiquita de las migas
?>
<div class="p-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
