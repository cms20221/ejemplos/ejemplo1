<?php

use app\models\S;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Suministradores';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="s-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Crear Suminitrador', 
                ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            's',
            'noms',
            //'estado',
            'ciudad',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, S $model, $key, $index, $column) {
                    return Url::toRoute([$action, 's' => $model->s]);
                 }
            ],
        ],
    ]); ?>


</div>
