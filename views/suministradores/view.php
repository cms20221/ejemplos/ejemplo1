<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/** @var yii\web\View $this */
/** @var app\models\S $model */

$this->title = "detalle Suministradores" . $model->s;
$this->params['breadcrumbs'][] = ['label' => 'Suministradores', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="s-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Actualizar', ['update', 's' => $model->s], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Eliminar', ['delete', 's' => $model->s], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Estas seguro que quieres eliminar?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            's',
            'noms',
            'estado',
            'ciudad',
        ],
    ]) ?>

</div>
