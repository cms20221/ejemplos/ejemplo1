<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\S $model */

$this->title = 'crear suministrador';
$this->params['breadcrumbs'][] = ['label' => 'Suminitradores', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="s-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
