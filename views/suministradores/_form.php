<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\S $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="s-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 's')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'noms')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'estado')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'ciudad')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Grabar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
