<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\J $model */

$this->title = 'Crear Proyecto';
$this->params['breadcrumbs'][] = ['label' => 'Js', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="j-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
