<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\J $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="j-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'j')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'nomj')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'ciudad')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'piezasTotales')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Grabar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
