<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\J $model */

$this->title = 'Modificando Proyecto ' . $model->j;
$this->params['breadcrumbs'][] = [
    'label' => 'Proyectos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->j, 'url' => ['view', 'j' => $model->j]];
$this->params['breadcrumbs'][] = 'actualizar';
?>
<div class="j-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
