<?php

use app\models\J;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = ''
        . 'Proyectos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="j-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('crear Proyecto', 
                ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'j',
            'nomj',
            'ciudad',
            //'piezasTotales',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, J $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'j' => $model->j]);
                 }
            ],
        ],
    ]); ?>


</div>
